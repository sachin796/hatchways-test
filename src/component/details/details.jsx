import React, { useState } from "react";
import Button from "../button/button.jsx";
import Image from "../imageComponent/image.jsx";
import TableData from "../table/table.jsx";
import Information from "../infoData/information.jsx";
import TagDiv from "../tagDiv/tagDiv.jsx";
import "./details.css";

function Details(props) {
  const [setActive, setActiveState] = useState("notactive");
  const [setHeight, setHeightState] = useState("0vh");

  function toggleGradeDivHeight(e) {
    e.preventDefault();
    setActiveState(setActive === "notactive" ? "active" : "notactive");
    setHeightState(setActive === "active" ? "0vh" : "50vh");
  }

  return (
    <>
      <div id="maindiv">
        <Image allprops={props} />
        <div id="textdiv">
          <Information allinfo={props} />
          <div id="gradesDiv" style={{ maxHeight: `${setHeight}` }}>
            {props.studData.grades.map((grade, index) => (
              <>
                <TableData grade={grade} index={index} />
              </>
            ))}
            <TagDiv tagDataprops={props} />
            <input
              type="text"
              className="add-tag-input"
              onKeyUp={props.addtag}
              id={props.index}
              placeholder="Add Tag Here (Press Enter After)"
            />
          </div>
        </div>
        <div className="expand-btn">
          <Button
            setActive={setActive}
            toggleGradeDivHeight={toggleGradeDivHeight}
          />
        </div>{" "}
      </div>
    </>
  );
}

export default Details;
