import React from "react";

function TableData(props) {
  return (
    <>
      <table>
        <thead></thead>
        <tbody>
          <tr>
            <td> Test {props.index} &nbsp;&nbsp;</td>
            <td>{props.grade}</td>
          </tr>
        </tbody>
      </table>
    </>
  );
}

export default TableData;
