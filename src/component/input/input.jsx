import React from "react";

function InputField(props) {
  return (
    <>
      <input
        id="name-input"
        type="text"
        onChange={props.searchClickevent}
        placeholder="Search"
      ></input>
    </>
  );
}

export default InputField;
