import React from "react";
import axios from "axios";
import Details from "../component/details/details.jsx";
import Search from "../component/searchOptions/search.jsx";

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      studentdata: null,
      filteredData: null,
      modifiedData: [],
    };
  }

  tagadd = (e) => {
    e.preventDefault();
    let index = e.target.id;
    let tagname = e.target.value;
    if (e.key === "Enter") {
      let studentdatacopy = JSON.parse(JSON.stringify(this.state.studentdata));
      studentdatacopy[index].tag.push(tagname);
      this.setState({
        studentdata: studentdatacopy,
      });
      e.target.value = "";
    }
  };

  searchClickevent = (e) => {
    e.preventDefault();
    let data = this.state.studentdata.filter((eachstudent) => {
      return (
        eachstudent.firstName.includes(e.target.value) ||
        eachstudent.lastName.includes(e.target.value)
      );
    });
    this.setState({ filteredData: data });
  };

  searchTag = (e) => {
    e.preventDefault();
    let filteredtagdata = this.state.studentdata.filter((eachstudent) => {
      return eachstudent.tag.includes(e.target.value);
    });
    this.setState({ filteredData: filteredtagdata });
  };

  componentDidMount() {
    axios
      .get("https://www.hatchways.io/api/assessment/students")
      .then(async (res) => {
        let updatedData = await res.data.students.map((studentData) => {
          studentData.tag = [];
          return studentData;
        });
        this.setState({
          studentdata: updatedData,
        });
      })
      .catch((err) => console.error(err));
  }
  render() {
    return (
      <>
        <Search
          className="searchDiv"
          datatestid="searchList"
          text="Search from list"
          searchClickevent={this.searchClickevent}
        />
        <Search
          className="tagDiv"
          datatestid="tagList"
          text="Search from tag"
          searchClickevent={this.searchTag}
        />
        {this.state.filteredData === null ||
        this.state.filteredData.length === 0 ? (
          <>
            {this.state.studentdata != null ? (
              <>
                {this.state.studentdata.map((student, index) => (
                  <>
                    <Details
                      studData={student}
                      index={index}
                      addtag={this.tagadd}
                    />
                  </>
                ))}
              </>
            ) : null}
          </>
        ) : (
          <>
            {this.state.filteredData.map((student) => (
              <>
                <Details studData={student} tagadd={this.addtag} />
              </>
            ))}
          </>
        )}
      </>
    );
  }
}
export default Main;
