import React from "react";

function Image(props) {
  return (
    <>
      <div
        id="imagediv"
        style={{ backgroundImage: `url(${props.allprops.studData.pic})` }}
      ></div>
    </>
  );
}

export default Image;
