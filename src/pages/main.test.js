import React from "react";
import Main from "./main.jsx";
import { render, cleanup } from "@testing-library/react";

afterEach(cleanup);

it("Displays text as expected", () => {
  const { getByTestId } = render(<Main />);
  expect(getByTestId("searchList").textContent).toBe("Search from list");
});

it("Displays text as expected", () => {
  const { getByTestId } = render(<Main />);
  expect(getByTestId("tagList").textContent).toBe("Search from tag");
});
