import React from "react";
import InputField from "../input/input.jsx";
function Search(props) {
  return (
    <>
      <div className={props.className} data-testid={props.datatestid}>
        {props.text}
        <InputField searchClickevent={props.searchClickevent} />
      </div>
    </>
  );
}

export default Search;
