import React from "react";

function TagDiv(props) {
  return (
    <>
      {props.tagDataprops.studData.tag.length !== 0 ? (
        <>
          <div className="tagdiv">
            {props.tagDataprops.studData.tag.map((tag) => (
              <>
                <div id="eachtag">{tag}</div>
              </>
            ))}
          </div>
        </>
      ) : null}
    </>
  );
}

export default TagDiv;
