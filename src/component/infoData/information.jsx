import React from "react";

function Information(props) {
  return (
    <>
      <h3>
        {`${props.allinfo.studData.firstName} ${props.allinfo.studData.lastName}`}
        {}
      </h3>
      <h6>Email :- {props.allinfo.studData.email}</h6>
      <h6>Company :- {props.allinfo.studData.company}</h6>
      <h6>Skill :- {props.allinfo.studData.skill}</h6>
      <h6>Average :- 75</h6>
      <br></br>
    </>
  );
}

export default Information;
