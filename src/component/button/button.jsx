import React from "react";

function Button(props) {
  return (
    <>
      <button
        className={`${props.setActive}`}
        onClick={props.toggleGradeDivHeight}
      >
        +
      </button>
    </>
  );
}

export default Button;
